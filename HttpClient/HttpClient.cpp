#include "HttpClient.h"

HttpClient::HttpClient() : curl(nullptr), res(CURLE_OK) {
	curl_global_init(CURL_GLOBAL_DEFAULT);
	initCurl();
}

HttpClient::~HttpClient() {
	cleanupCurl();
	curl_global_cleanup();
}

void HttpClient::initCurl()
{
	curl = curl_easy_init();
	if (!curl)
		throw std::runtime_error("Failed to initialize cURL");
}

void HttpClient::cleanupCurl()
{
	if (curl)
		curl_easy_cleanup(curl);
}

size_t HttpClient::writeCallback(void* contents, size_t size, size_t nmemb, std::string* s)
{
	size_t totalSize = size * nmemb;
	s->append(static_cast<char*>(contents), totalSize);
	return totalSize;
}

std::string HttpClient::get(const std::string& url)
{
	std::string response;

	curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 0);
	curl_easy_setopt(curl, CURLOPT_URL, url.c_str());
	curl_easy_setopt(curl, CURLOPT_HTTPAUTH, (long)CURLAUTH_BASIC);
	curl_easy_setopt(curl, CURLOPT_USERNAME, "jschmidt");
	curl_easy_setopt(curl, CURLOPT_PASSWORD, "");
	curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, writeCallback);
	curl_easy_setopt(curl, CURLOPT_WRITEDATA, &response);

	struct curl_slist *headers = NULL;
	headers = curl_slist_append(headers, "Accept: application/json");
	curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headers);

	res = curl_easy_perform(curl);
	if (res != CURLE_OK)
		throw std::runtime_error(curl_easy_strerror(res));

	return response;
}

