#ifndef HTTPCLIENT_H
#define HTTPCLIENT_H

#include <curl/curl.h>
#include <string>
#include <stdexcept>

class HttpClient {
public:
	HttpClient();
	~HttpClient();

	std::string get(const std::string& url);
	std::string post(const std::string& url, const std::string& data);

private:
	static size_t writeCallback(void* contents, size_t size, size_t nmemb, std::string* s);
	void initCurl();
	void cleanupCurl();

	CURL* curl;
	CURLcode res;
};

#endif // HTTPCLIENT_H
